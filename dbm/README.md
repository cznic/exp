dbm
===

Package dbm implements a simple database engine, a hybrid of a hierarchical
and/or a key-value one.

Installation: $ go get modernc.org/exp/dbm

Documentation: [godoc.org/modernc.org/exp/dbm](http://godoc.org/modernc.org/exp/dbm)
