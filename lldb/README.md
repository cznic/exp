lldb
====

**Deprecated**

2016-07-11: This package is now officially deprecated. It will be kept here because there are few brave people using it, but there will be no future updates to this package. Please switch to [godoc.org/modernc.org/lldb](http://godoc.org/modernc.org/lldb)

----

Package lldb (WIP) implements a low level database engine.

Installation: $ go get modernc.org/exp/lldb

Documentation: [godoc.org/modernc.org/exp/lldb](http://godoc.org/modernc.org/exp/lldb)
